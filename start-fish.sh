#!/bin/bash

echo 'starting fish'
fish -C 'alias d=docker; alias dc=docker-compose; source venv/bin/activate.fish'